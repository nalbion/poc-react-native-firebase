This is a simple proof of concept a [React Native](https://facebook.github.io/react-native/) mobile app (for iOS and Android).

Usage:

- User installs app, is prompted to log into Google; the app obtains a [Firebase Auth token](https://firebase.google.com/docs/auth/).
  - The POC app has no UI other than the auth flow
- The app goes into background mode, allowing user to use other apps and even reboot the phone.
- backend (can use Firebase web console to test) sends [message](https://firebase.google.com/docs/cloud-messaging/)


    { echo: "random string" }
	
When the RTM payload does not include "url" the app echos to Firebase 
(or adds an entry to Firebase DB - not really important at this stage, 
just proving that the platform works for what I want it to do in future phase (out of scope)


    {url: "http://google.com?q=cats" }

When the RTM payload does include "url", the app shows the web page in a WebView